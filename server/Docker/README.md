# SOBRE ESTE CONTENEDOR

Este repositorio tiene como base el repositorio oficial de blynk en donde proporcionan una configuración para un contenedor que ejecuta su servidor. Fue modificado para lograr un servidor seguro con SSL auto del propio server además de lograr redirección de puertos para challenge de Let's Encrypt.

## Como correrlo

- Clonar este repositorio

- Compilarlo en la raiz donde está el archivo Dockerfile con el siguiente comando.

```bash
docker build -t blynk2 .
```

- Ejecutar el servidor como contenedor con el siguiente comando: 

```bash
docker run --name blynk-server -v ~/blynk-server/server/Docker:/data -p 8440:8440 -p 8080:8080 -p 9443:9443 -p 80:8080 -p 8081:8081 -p 8082:8082 blynk2 &
```

Si se quieren modificar las variables de entorno por ejemplo para añadir un servidor SMTP para enviar por correo los token se debe modificar el archivo bin/run.sh

```bash
sudo nano bin/run.sh
```


## HOW TO 

### Configure

Just edit the Dockerfile and change ENV vars


### Build

```bash
docker build -t blynk .
```

### RUN

```bash
docker run --name blynk-server -v ~/blynk-server/server/Docker:/data -p 8440:8440 -p 8080:8080 -p 9443:9443 blynk &
```

Don't forget to change the port attribution if you change on the ENV vars in Dockerfile


## UPDATE Blynk version

## How to

Stop and remove your actual container

```bash
docker stop blynk-server && docker rm blynk-server
```

- Edit your Blynk server version on the ENV var in Dockerfile
- Build & Launch


Have Fun ! :v: :whale:

